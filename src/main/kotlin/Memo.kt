import java.util.Stack

class Memo {
    private val history = Stack<MemoAction>()
    private val present = HashMap<String, String>()

    operator fun plusAssign(action: MemoAction) {
        history.push(action)

        when (action.action) {
            MemoOperation.SET -> present[action.name] = action.value
            MemoOperation.REMOVE -> present.remove(action.name)
            MemoOperation.APPEND -> present[action.name] = present.getOrDefault(action.name, "") + action.value
        }
    }

    fun pop(count: Int) {
        present.clear()

        repeat(count) {
            history.pop()
        }

        for (event in history) {
            when (event.action) {
                MemoOperation.SET -> present[event.name] = event.value
                MemoOperation.REMOVE -> present.remove(event.name)
                MemoOperation.APPEND -> present[event.name] = present.getOrDefault(event.name, "") + event.value
            }
        }
    }

    operator fun get(key: String) = present[key]

    fun containsKey(key: String) = present.containsKey(key)

    fun clear() {
        history.clear()
        present.clear()
    }

    val size: Int
        get() = present.size

    val eventSize: Int
        get() = history.size
}