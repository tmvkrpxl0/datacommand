import imgui.ImGui
import imgui.app.Application
import imgui.app.Configuration
import imgui.type.ImInt
import imgui.type.ImString
import kotlinx.coroutines.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.lang.Integer.min

val memo = Memo()
var suggestions: MutableList<Pair<DataNode, ImproveSuggestion>> = mutableListOf()
val standardOutput: MutableList<String> = mutableListOf()
val errorOutput: MutableList<String> = mutableListOf()
val analyzedData: MutableList<Pair<IntRange, DataNode>> = mutableListOf()
var invalid = false
var textField = ImString()
var previous: String = textField.get()

val tips = mutableListOf<Tip>()

val absoluteRoot = DataNode("absoluteroot", LiteralToken(""), description = "absolute root node, need for analyzing")

fun main() {
    val gui = Gui()
    reload()
    analyze()
    Application.launch(gui)
}

fun analyze() {
    val input = textField.get()

    if (analyzedData.size > 0) {
        val last = analyzedData.removeLast()
        memo.pop(last.second.memoActions.size)
    }

    var currentNode = analyzedData.lastOrNull()?.second ?: absoluteRoot
    var analyzeFrom = analyzedData.lastOrNull()?.first?.last?.plus(1) ?: 0
    var textPiece = input.substring(analyzeFrom)
    analyzeLoop@ while (true) {
        if (textPiece == "" && currentNode != absoluteRoot) break@analyzeLoop // 입력 전부 분석 끝

        val available = currentNode.getAvailable(memo)

        val valid = mutableListOf<Pair<CharSequence, DataNode>>()
        for (length in 0..textPiece.length) {
            val toTest = textPiece.substring(0, length)
            available.filter { node ->
                node.token.strictlyThis(toTest)
            }.forEach {
                valid += toTest to it
            }
        }

        val longest = valid.maxByOrNull { it.first.length }
        if (longest != null) {
            textPiece = textPiece.substring(longest.first.length)
            currentNode = longest.second
            currentNode.memoActions.forEach { it.operate(memo) }
            val previousIndex = analyzeFrom
            analyzeFrom += longest.first.lastIndex
            analyzedData += previousIndex..analyzeFrom to currentNode
            analyzeFrom++
        } else {
            break@analyzeLoop // 알 수 없는 노드 발견, 자동완성 제안으로 넘어감
        }
    }

    suggestions.clear()

    fun getSuggestions(node: DataNode, piece: String): List<Pair<DataNode, ImproveSuggestion>> {
        val available = node.getAvailable(memo)
        return available
            .map { it to it.token.couldBeThis(piece) }
            .filter { it.second != null } as List<Pair<DataNode, ImproveSuggestion>>
    }

    if (analyzedData.size >= 2) {
        val secondLast = analyzedData[analyzedData.lastIndex - 1]

        val previousNode = secondLast.second
        val piece = input.substring(min(secondLast.first.last.plus(1), input.lastIndex))

        memo.pop(currentNode.memoActions.size)
        suggestions += getSuggestions(previousNode, piece)
        currentNode.memoActions.forEach { it.operate(memo) }

        suggestions += getSuggestions(currentNode, textPiece)
    } else if (analyzedData.size == 1) {
        val previousNode = absoluteRoot
        val piece = input

        memo.pop(currentNode.memoActions.size)
        suggestions += getSuggestions(previousNode, piece)
        currentNode.memoActions.forEach { it.operate(memo) }

        suggestions += getSuggestions(currentNode, textPiece)
    } else {
        suggestions += getSuggestions(currentNode, textPiece)
    }

    invalid = textPiece != "" && suggestions.isEmpty()
}
fun reload() {
    absoluteRoot.connections.clear()
    tips.clear()

    try {
        val commands = File("src/main/resources").listFiles()!!
            .filter { it.name.endsWith(".json") }
            .map { Json.decodeFromString<Command>(it.readText()) }
            .toTypedArray()

        commands.forEach { command ->
            command.nodes.forEach { node ->
                node.connections.forEach { connection ->
                    connection.populate(command.nodes)
                }
            }

            absoluteRoot.connections += NodeConnection(command.rootNode)
            tips += command.tips
        }
        errorOutput.clear()
    } catch (e: Exception) {
        errorOutput.clear()
        errorOutput += e.message?: ""
    }

    memo.clear()
    analyzedData.clear()
    try {
        analyze()
    } catch (e: Exception) {
        println(e.message)
        e.printStackTrace()
    }
}

class Gui : Application() {
    override fun configure(config: Configuration?) {
        super.configure(config)
    }

    override fun initImGui(config: Configuration?) {
        super.initImGui(config)
        ImGui.getIO().run {
            fonts.addFontFromFileTTF("src/main/resources/NanumGothic.ttf", 15F, fonts.glyphRangesKorean)
            fonts.getTexDataAsRGBA32(ImInt(18), ImInt(18))
        }
    }

    override fun process() {
        if (ImGui.inputText("type here", textField)) {
            val common = previous.commonPrefixWith(textField.get())
            if (common != previous) {
                val toRemove = analyzedData.filter { it.first.last > common.lastIndex }
                memo.pop(toRemove.sumOf { it.second.memoActions.size })
                analyzedData.removeAll(toRemove)
            }
            try {
                analyze()
            } catch (e: Exception) {
                println(e.message)
                e.printStackTrace()
            }

            previous = textField.get()
        }
        tips.forEach { cm ->
            if (cm.`when`.check(memo)) ImGui.textColored(255, 255, 0, 255, cm.message)
        }

        for ((node, suggestion) in suggestions) {
            val line = "\"${node.token}\"     ${node.description ?: ""}"
            ImGui.bulletText(line)
            suggestion.message.forEach {
                ImGui.textWrapped(it)
            }
        }

        if (invalid) {
            ImGui.textColored(255, 204, 0, 255, "Invalid command!")
        }

        if (ImGui.button("reload")) {
            reload()
        }
        if (ImGui.button("execute") && textField.get() != "") {
            val runtime = Runtime.getRuntime()
            val process = runtime.exec(textField.get().replace("\"", "")) // TODO 명령어에서 따옴표 제거하는 방법 찾아야 하나?

            runBlocking {
                val out = process.inputStream.bufferedReader()
                val error = process.errorStream.bufferedReader()
                launch(Dispatchers.IO) {
                    standardOutput.clear()
                    standardOutput += ">${textField.get()}"
                    standardOutput += out.readLines()
                    if (standardOutput.isEmpty()) standardOutput += "There was no output"
                }
                launch(Dispatchers.IO) {
                    errorOutput.clear()
                    errorOutput += error.readLines()
                }

                launch(Dispatchers.IO) {
                    process.waitFor()
                }
            }
        }

        for (output in standardOutput) {
            ImGui.bulletText(output)
        }

        ImGui.newLine()
        ImGui.newLine()

        for (output in errorOutput) {
            ImGui.textColored(255, 0, 0, 255, output)
        }
    }
}