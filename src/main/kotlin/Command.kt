import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.io.File

@Serializable
data class Command(
    @SerialName("name") val commandName: String,
    @SerialName("nodes") val nodes: List<DataNode>,
    @SerialName("root_id") val rootIdentifier: String,
    @SerialName("tips") val tips: List<Tip> = emptyList()
) {
    val rootNode: DataNode by lazy { nodes.find { it.identifier == rootIdentifier }!! }
}

@Serializable
data class Tip(
    val `when`: ICommandPredicate,
    val message: String
)

@Serializable
data class DataNode(
    val identifier: String,
    val token: IToken,
    val connections: MutableList<NodeConnection> = mutableListOf(),
    val description: String? = null,
    var memoActions: MutableList<MemoAction> = mutableListOf(),
    val conditions: MutableList<ICommandPredicate> = mutableListOf(),
) {
    fun getAvailable(memo: Memo): List<DataNode> {
        return connections
            .filter { connection -> connection.conditions.firstOrNull { !it.check(memo) } == null }
            .map { it.toNode }
            .filter { node -> node.conditions.firstOrNull { !it.check(memo) } == null }
    }

    fun toThis() = NodeConnection(identifier)
}

@Serializable
data class MemoAction(
    val action: MemoOperation,
    val name: String,
    val value: String = ""
) {
    fun operate(memo: Memo) {
        memo += this
    }
}

@Serializable
enum class MemoOperation {
    @SerialName("put")
    SET,

    @SerialName("remove")
    REMOVE,

    @SerialName("append")
    APPEND
}

@JvmInline
value class ImproveSuggestion(val message: List<String>)

@Serializable
sealed interface IToken {
    /**
     * 체크 실패시 null 반환
     * 체크 성공시 null 아닌 값 반
     * 만약 toTest 가 너무나 완벽하여 제안할만한게 없다면 빈 문자열 반환해도 됨
     */
    fun couldBeThis(toTest: String): ImproveSuggestion? = ImproveSuggestion(emptyList())

    fun strictlyThis(toTest: String): Boolean = couldBeThis(toTest) != null
}

@Serializable
@SerialName("literal")
data class LiteralToken(
    val literal: String
) : IToken {
    override fun couldBeThis(toTest: String): ImproveSuggestion? {
        if (!literal.startsWith(toTest)) return null

        return ImproveSuggestion(emptyList())
    }

    override fun strictlyThis(toTest: String) = literal == toTest

    override fun toString() = literal
}

@Serializable
@SerialName("number")
data class NumberToken(
    @SerialName("only_integer") val integerOnly: Boolean = false
) : IToken {
    override fun couldBeThis(toTest: String): ImproveSuggestion? {
        if (toTest == "") return ImproveSuggestion(listOf("<숫자>"))

        return if (strictlyThis(toTest)) ImproveSuggestion(emptyList()) else null
    }

    override fun strictlyThis(toTest: String): Boolean {
        return if (integerOnly) {
            toTest.toIntOrNull() != null
        } else {
            toTest.toDoubleOrNull() != null
        }
    }

    override fun toString(): String {
        return if (integerOnly) "<정수>" else "<숫자>"
    }
}

// TODO 현재 문제: 알고리즘 구조상 /media/tmvkrpxl0 과 /media 둘다 말이 되는 경우를 제대로 처리하지 못함
//  /media 를 하나의 토큰으로 보고 tmvkrpxl0 과 분리해 버리는 문제가 생김
@Serializable
@SerialName("file")
data class FileToken(
    @SerialName("allow_dir") val allowDir: Boolean = false,
    @SerialName("allow_file") val allowFile: Boolean = true,
    @SerialName("allow_devfile") val allowDevFile: Boolean = false
) : IToken {

    init {
        assert(allowDir || allowFile || allowDevFile) { "FileToken 이 아무런 형식의 파일도 받지 않습니다!" }
    }
    override fun couldBeThis(toTest: String): ImproveSuggestion? {
        val isDirSpecified = toTest.endsWith('/')
        val toTestAbsolute = if (toTest.startsWith('/')) File(toTest) else File("./").absoluteFile.resolve(toTest)

        val searchFrom = if (isDirSpecified) toTestAbsolute else toTestAbsolute.parentFile

        if (!searchFrom.exists()) return null

        assert(searchFrom.isDirectory)

        val children = searchFrom.listFiles()!!

        if (children.isEmpty()) return ImproveSuggestion(listOf("빈 폴더 입니다."))

        val candidates = children
            .filter {
                if (it.isDirectory) return@filter true
                if (allowFile && it.isFile) return@filter true
                if (!it.isFile && !it.isDirectory && allowDevFile) return@filter true // TODO 아마 이거 더 정확히 확인할 수 있을텐데..
                return@filter false
            }.filter {
                isDirSpecified || it.name.startsWith(toTestAbsolute.name)
            }.map {
                it.name
            }

        return if (candidates.isEmpty()) null
        else ImproveSuggestion(candidates)
    }

    override fun strictlyThis(toTest: String): Boolean {
        val file = File(toTest)

        if (!file.exists()) return false

        if (allowDir && file.isDirectory) return true
        if (allowFile && file.isFile) return true
        if (!file.isFile && !file.isDirectory && allowDevFile) return true // TODO 아마 이거 더 정확히 확인할 수 있을텐데..

        return false
    }

    private var cachedString: String? = null
    override fun toString(): String {
        if (cachedString == null) {
            val builder = mutableListOf<String>()

            if (allowDir) builder += "디렉터리"
            if (allowFile) builder += "파일"
            if (allowDevFile) builder += "디바이스 파일"

            if (builder.size == 2) {
                builder.add(1, "|")
            } else if (builder.size == 3) {
                builder.add(3, "|")
            }
            builder.add(0, "<")
            builder.add(">")

            cachedString = builder.joinToString("")
        }
        return cachedString!!
    }
}

@Serializable
@SerialName("string")
object StringToken : IToken {

    override fun couldBeThis(toTest: String): ImproveSuggestion? {
        if (toTest == "") {
            return ImproveSuggestion(listOf("\"문자열\""))
        }
        if (toTest.startsWith('\"') && !toTest.endsWith('\"')) {
            return ImproveSuggestion(listOf("$toTest\""))
        }
        if (toTest.startsWith('\"') && toTest.endsWith('\"')) {
            return ImproveSuggestion(emptyList())
        }
        return null
    }

    override fun strictlyThis(toTest: String): Boolean {
        return toTest.length > 1 && toTest.startsWith('\"') && toTest.endsWith('\"')
    }

    override fun toString() = "\"<문자열>\""
}

@Serializable
data class NodeConnection(
    val to: String,
    val conditions: MutableList<ICommandPredicate> = mutableListOf(),
) {
    constructor(
        toNode: DataNode,
        conditions: MutableList<ICommandPredicate> = mutableListOf()
    ) : this(toNode.identifier, conditions) {
        _toNode = toNode
    }

    private var _toNode: DataNode? = null

    fun populate(nodes: List<DataNode>) {
        val tryFind = nodes.find { it.identifier == to } ?: throw NullPointerException("Node with id $to is missing!")
        _toNode = tryFind
    }

    val toNode: DataNode by lazy { _toNode!! }
}

@Serializable
sealed interface ICommandPredicate {
    fun check(target: Memo): Boolean
}

@Serializable
@SerialName("has")
data class Has(
    @SerialName("name")
    val nameInMemo: String,
) : ICommandPredicate {
    override fun check(target: Memo) = target.containsKey(nameInMemo)
}

@Serializable
@SerialName("equals")
data class EqualTo(
    @SerialName("name")
    val nameInMemo: String,
    @SerialName("expected")
    val compareWith: String,
) : ICommandPredicate {
    override fun check(target: Memo) = target[nameInMemo] == compareWith
}

@Serializable
@SerialName("or")
data class Or(
    val predicate1: ICommandPredicate,
    val predicate2: ICommandPredicate
) : ICommandPredicate {
    override fun check(target: Memo): Boolean {
        if (predicate1 == this || predicate2 == this) throw IllegalStateException("재귀 OR 로직 감지됨!")
        return predicate1.check(target) || predicate2.check(target)
    }
}

@Serializable
@SerialName("contains")
data class Contains(
    @SerialName("name")
    val nameInMemo: String,
    val pattern: String,
) : ICommandPredicate {
    override fun check(target: Memo) = target[nameInMemo]?.contains(pattern) == true

}

@Serializable
@SerialName("and")
data class And(
    val predicate1: ICommandPredicate,
    val predicate2: ICommandPredicate
) : ICommandPredicate {
    override fun check(target: Memo): Boolean {
        if (predicate1 == this || predicate2 == this) throw IllegalStateException("재귀 AND 로직 감지됨!")
        return predicate1.check(target) && predicate2.check(target)
    }
}

@Serializable
@SerialName("not")
data class Not(
    @SerialName("to_invert")
    val toInvert: ICommandPredicate,
) : ICommandPredicate {
    override fun check(target: Memo): Boolean {
        if (toInvert == this) throw IllegalStateException("재귀 NOT 로직 감지됨!")
        return !toInvert.check(target)
    }
}