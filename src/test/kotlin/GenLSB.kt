import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import java.io.File

class GenLSB {
    @Test
    fun genLSB() {
        val nodes = mutableListOf<DataNode>()
        val rootNode = DataNode("lsb_release", "lsb_release ".toLiteral(), description = "Prints LSB module info").also { lsb ->
            val optionSpace = DataNode("option_space", " ".toLiteral())
            nodes += optionSpace
            nodes += lsb

            literalSharedMemo("help", "help",
                "-h" to "show this help message and exit",
                "--help" to "show this help message and exit",
                nodes = nodes,
                optionSpace = optionSpace,
                connectBackToSpace = false,
                connectFromSpace = false,
                lsbNode = lsb
            )

            literalSharedMemo("version", "version",
                "-v" to "show LSB modules this system supports",
                "--version" to "show LSB modules this system supports",
                nodes = nodes,
                optionSpace = optionSpace,
                connectBackToSpace = false,
                connectFromSpace = false,
                lsbNode = lsb
            )

            literalSharedMemo("id", "id",
                "-i" to "show distributor ID",
                "--id" to "show distributor ID",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            )

            literalSharedMemo("description", "description",
                "-d" to "show description of this distribution",
                "--description" to "show description of this distribution",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            )

            literalSharedMemo("release", "release",
                "-r" to "show release number of this distribution",
                "--release" to "show release number of this distribution",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            )

            literalSharedMemo("codename", "codename",
                "-c" to "show code name of this distribution",
                "--codename" to "show code name of this distribution",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            )

            literalSharedMemo("all", "all",
                "-a" to "show all of the above information",
                "--all" to "show all of the above information",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            ) {
                memoActions += MemoAction(MemoOperation.SET, "codename")
                memoActions += MemoAction(MemoOperation.SET, "release")
                memoActions += MemoAction(MemoOperation.SET, "description")
                memoActions += MemoAction(MemoOperation.SET, "id")
            }

            literalSharedMemo("short", "short",
                "-s" to "show requested information in short format",
                "--short" to "show requested information in short format",
                nodes = nodes,
                optionSpace = optionSpace,
                lsbNode = lsb
            )
        }
        val command = Command("lsb_release", nodes, rootNode.identifier, listOf(
            Tip(
                Has("short"),
                "This will prints requested information in short format"
            )
        ))

        File("src/test/resources/lsb_release.json").writeText(Json.encodeToString(command))
    }
}

private fun String.toLiteral() = LiteralToken(this)

private fun literalSharedMemo(
    identifierBaseName: String,
    memoKey: String,
    vararg literals: Pair<String, String?>,
    nodes: MutableList<DataNode>,
    optionSpace: DataNode,
    connectBackToSpace: Boolean = true,
    connectFromSpace: Boolean = true,
    lsbNode: DataNode,
    init: DataNode.() -> Unit = {}
): List<DataNode> {
    return literals.mapIndexed { index, literal ->
        val indexTag = if (literals.isEmpty()) "" else "_$index"
        val node = DataNode("$identifierBaseName$indexTag", literal.first.toLiteral(), description = literal.second)

        lsbNode.connections += node.toThis()
        if (connectFromSpace) optionSpace.connections += node.toThis()
        if (connectBackToSpace) node.connections += optionSpace.toThis()

        node.memoActions += MemoAction(MemoOperation.SET, memoKey)
        node.conditions += Not(Has(memoKey))

        node.init()

        nodes += node

        node
    }
}