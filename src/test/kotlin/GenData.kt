import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import kotlin.test.Test

// 테스트 사용법들
// test a 정수
// test b 문자열
// test b 문자열 <다른것들>
// test -v
// test -i a정수
// test -i b문자열
class GenData {
    @Test
    fun generate() {
        val nodes = mutableListOf<DataNode>()

        val rootNode = DataNode("root", "test".toLiteral(), description = "test command").also { root ->
            nodes += root
            DataNode("space", " ".toLiteral(), description = "space in between every arguments").also { space ->
                nodes += space
                root.connections += space.toThis()
                space.conditions += Not(Has("printVersion"))

                DataNode("a", "a".toLiteral(), description = "test a argument").also { a ->
                    nodes += a
                    space.connections += a.toThis()

                    DataNode("aspace", LiteralToken(" "), description = "Between a and integer").also { aspace ->
                        nodes += aspace
                        a.connections += aspace.toThis()

                        DataNode("integer", NumberToken(true), description = "integer node for a").also { integer ->
                            nodes += integer
                            aspace.connections += integer.toThis()
                        }
                    }
                }

                DataNode("b", "b".toLiteral(), description = "test b argument").also { b ->
                    nodes += b
                    space.connections += NodeConnection(b.identifier)

                    DataNode("bspace", LiteralToken(" "), description = "Between b and string").also { bspace ->
                        nodes += bspace
                        b.connections += bspace.toThis()

                        DataNode("string", StringToken, description = "string token for b").also { string ->
                            nodes += string
                            bspace.connections += string.toThis()

                            string.connections += space.toThis()
                        }
                    }

                    b.memoActions += MemoAction(MemoOperation.SET, "used_b", "true")

                    b.conditions += Not(Has("used_b"))
                }

                DataNode("version", "-v".toLiteral(), description = "test version argument").also { version ->
                    nodes += version
                    space.connections += NodeConnection(version.identifier)

                    val versionMemoAction = MemoAction(MemoOperation.SET, "printVersion", "true")
                    version.memoActions += versionMemoAction
                }

                DataNode("ignored node", "-i".toLiteral(), description = "this argument is ignored, can only be used once").also { i ->
                    nodes += i
                    space.connections += NodeConnection(i.identifier)

                    i.memoActions += MemoAction(MemoOperation.SET, "used_i", "true")

                    i.conditions += Not(Has("used_i"))

                    i.connections += NodeConnection(space.identifier)
                }
            }
        }
        val command = Command("test", nodes, rootNode.identifier)

        File("src/test/resources/test.json").writeText(Json.encodeToString(command))
    }
}

private fun String.toLiteral() = LiteralToken(this)