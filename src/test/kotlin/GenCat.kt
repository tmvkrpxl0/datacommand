import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import java.io.File

class GenCat {
    @Test
    fun genCat() {
        val nodes = mutableListOf<DataNode>()
        val root = DataNode("cat", "cat ".toLiteral(), description = "Read files into stdout").also { cat ->
            nodes += cat
            DataNode("option_space", " ".toLiteral()).also { optionSpace ->
                nodes += optionSpace

                literalSharedMemo("nonblank", "nonblank",
                "-b" to "number nonempty output lines, overrides -n",
                "--number-nonblank" to "number nonempty output lines, overrides -n",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                )

                literalSharedMemo("show_ends", "show_ends",
                    "-E" to "display \$ at end of each line",
                    "--show-ends" to "display \$ at end of each line",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                )

                literalSharedMemo("number", "number",
                    "-n" to "number all output lines",
                    "--number" to "number all output lines",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                ) {
                    conditions += Not(Or(Has("nonblank_0"), Has("nonblank_1")))
                }

                literalSharedMemo("squeeze", "squeze",
                    "-s" to "suppress repeated empty output lines",
                    "--squeeze-blank" to "suppress repeated empty output lines",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                )

                literalSharedMemo("show_tabs", "show_tabs",
                    "-T" to "Show tab character as ^I",
                    "--show-tabs" to "Show tab character as ^I",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                )

                literalSharedMemo("show_nonprinting", "show_nonprinting",
                    "-v" to "Uses ^ and M- expression",
                    "--show-nonprinting" to "Uses ^ and M- expression",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                )

                literalSharedMemo("show_all", "show_all",
                    "-A" to "equivalent to -vET",
                    "--show-all" to "equivalent to -vET",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                ) {
                    memoActions += MemoAction(MemoOperation.SET, "show_nonprinting")
                    memoActions += MemoAction(MemoOperation.SET, "show_ends")
                    memoActions += MemoAction(MemoOperation.SET, "show_tabs")
                }

                literalSharedMemo("show_expression_verbose", "show_expression_verbose",
                    "-e" to "equivalent to -vE",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                ) {
                    memoActions += MemoAction(MemoOperation.SET, "show_nonprinting")
                    memoActions += MemoAction(MemoOperation.SET, "show_ends")
                }

                literalSharedMemo("show_tabs_verbose", "show_tabs_verbose",
                    "-t" to "equivalent to -vT",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    catNode = cat
                ) {
                    memoActions += MemoAction(MemoOperation.SET, "show_nonprinting")
                    memoActions += MemoAction(MemoOperation.SET, "show_tabs")
                }

                DataNode("help", "--help".toLiteral(), description = "Prints help and exits").also { help ->
                    nodes += help
                    cat.connections += help.toThis()
                }

                DataNode("version", "--version".toLiteral(), description = "Prints version info and exits").also { version ->
                    nodes += version
                    cat.connections += version.toThis()
                }

                val stdin = DataNode("stdin", "-".toLiteral(), description = "Read from stdin")
                nodes += stdin
                cat.connections += stdin.toThis()
                optionSpace.connections += stdin.toThis()
                stdin.memoActions += MemoAction(MemoOperation.SET, "read_stdin")

                DataNode("file", FileToken(false, true, true), description = "File to print").also { file ->
                    nodes += file
                    cat.connections += file.toThis()
                    optionSpace.connections += file.toThis()

                    DataNode("file_space", " ".toLiteral()).also { fileSpace ->
                        nodes += fileSpace
                        file.connections += fileSpace.toThis()
                        fileSpace.connections += file.toThis()

                        stdin.connections += fileSpace.toThis()
                        fileSpace.connections += stdin.toThis()
                    }
                }
            }
        }
        val command = Command(
            "cat",
            nodes,
            root.identifier,
            tips = listOf(
                Tip(Has("read_stdin"), "This will read from stdin, which will require user input. Execute button below will fail.")
            )
        )

        File("src/test/resources/cat.json").writeText(Json.encodeToString(command))
    }
}

private fun String.toLiteral() = LiteralToken(this)

private fun literalSharedMemo(
    identifierBaseName: String,
    memoKey: String,
    vararg literals: Pair<String, String?>,
    nodes: MutableList<DataNode>,
    optionSpace: DataNode,
    connectBackToSpace: Boolean = true,
    catNode: DataNode,
    init: DataNode.() -> Unit = {}
): List<DataNode> {
    return literals.mapIndexed { index, literal ->
        val indexTag = if (literals.isEmpty()) "" else "_$index"
        val node = DataNode("$identifierBaseName$indexTag", literal.first.toLiteral(), description = literal.second)

        catNode.connections += node.toThis()
        optionSpace.connections += node.toThis()
        if (connectBackToSpace) node.connections += optionSpace.toThis()

        node.memoActions += MemoAction(MemoOperation.SET, memoKey)
        node.conditions += Not(Has(memoKey))

        node.init()

        nodes += node

        node
    }
}