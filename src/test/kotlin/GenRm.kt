import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import java.io.File

class GenRm {
    @Test
    fun genRm() {
        val nodes = mutableListOf<DataNode>()
        val rootNode = DataNode("rm", "rm ".toLiteral(), description = "Remove or unlink files").also { rm ->
            nodes += rm

            DataNode("option_space", " ".toLiteral()).also { optionSpace ->
                nodes += optionSpace

                literalSharedMemo(
                    "force", "has_force",
                    "-f" to "ignore nonexistent files and arguments, never prompt",
                    "--force" to "ignore nonexistent files and arguments, never prompt",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm,
                )

                literalSharedMemo(
                    "prompt", "do_prompt",
                    "-i" to "prompt before every removal",
                    "-I" to "prompt once before removing more than three files, or when removing recursively",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm
                )

                literalSharedMemo(
                    "prompt_2", "do_prompt",
                    "--interactive" to "prompt according to [never|once|always]",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm
                ) {
                    val never =
                        DataNode("prompt_never", "=never".toLiteral(), description = "never prompt when deleting")
                    val once = DataNode("prompt_once", "=once".toLiteral(), description = "once prompt when deleting")
                    val always =
                        DataNode("prompt_always", "=always".toLiteral(), description = "always prompt when deleting")

                    val interactive = this
                    val initializer: DataNode.() -> Unit = {
                        nodes += this

                        interactive.connections += toThis()
                        connections += optionSpace.toThis()
                    }

                    never.initializer()
                    once.initializer()
                    always.initializer()
                }

                literalSharedMemo(
                    "no_preserve_root", "preserve_root",
                    "--no-preserve-root" to "do not treat '/' specially",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm,
                )

                literalSharedMemo(
                    "preserve_root", "preserve_root",
                    "--preserve-root" to "do not remove '/'",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm,
                ) {
                    val all = DataNode(
                        "preserve_root_all",
                        "=all".toLiteral(),
                        description = "reject any command line argument on a separate device from its parent"
                    )

                    nodes += all
                    connections += all.toThis()
                    all.connections += optionSpace.toThis()
                }

                literalSharedMemo("recursive", "recursive",
                    "-r" to "remove directories and their contents recursively",
                    "-R" to "remove directories and their contents recursively",
                    "--recursive" to "remove directories and their contents recursively",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm
                )

                literalSharedMemo("dir", "dir",
                    "-d" to "remove empty directories",
                    "--dir" to "remove empty directories",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm
                )

                literalSharedMemo("verbose", "verbose",
                    "-v" to "explain what is being done",
                    "--verbose" to "explain what is being done",
                    nodes = nodes,
                    optionSpace = optionSpace,
                    rmNode = rm
                )

                DataNode("version_0", "-v".toLiteral(), description = "prints version info and exits").also { version ->
                    nodes += version

                    rm.connections += version.toThis()
                }

                DataNode("version_1", "--version".toLiteral(), description = "prints version info and exits").also { version ->
                    nodes += version

                    rm.connections += version.toThis()
                }

                DataNode("help_0", "-h".toLiteral(), description = "prints help message and exits").also { help ->
                    nodes += help

                    rm.connections += help.toThis()
                }

                DataNode("help_1", "--help".toLiteral(), description = "prints help message and exits").also { help ->
                    nodes += help

                    rm.connections += help.toThis()
                }

                val deleteFile = DataNode("delete_file", FileToken(false, true, false))
                val deleteDir = DataNode("delete_dir", FileToken(true, false, false))
                val spaceBetweenTargets = DataNode("space_between_targets", " ".toLiteral())
                nodes += spaceBetweenTargets

                val initializer: DataNode.() -> Unit = {
                    nodes += this

                    optionSpace.connections += toThis()
                    connections += spaceBetweenTargets.toThis()
                    rm.connections += toThis()
                }

                deleteFile.initializer()
                deleteDir.initializer()

                deleteDir.conditions += Has("dir")
            }
        }

        val command = Command("rm", nodes, rootNode.identifier)

        File("src/test/resources/rm.json").writeText(Json.encodeToString(command))
    }
}

private fun literalSharedMemo(
    identifierBaseName: String,
    memoKey: String,
    vararg literals: Pair<String, String?>,
    nodes: MutableList<DataNode>,
    optionSpace: DataNode,
    connectBackToSpace: Boolean = true,
    rmNode: DataNode,
    init: DataNode.() -> Unit = {}
): List<DataNode> {
    return literals.mapIndexed { index, literal ->
        val indexTag = if (literals.isEmpty()) "" else "_$index"
        val node = DataNode("$identifierBaseName$indexTag", literal.first.toLiteral(), description = literal.second)

        rmNode.connections += node.toThis()
        optionSpace.connections += node.toThis()
        if (connectBackToSpace) node.connections += optionSpace.toThis()

        node.memoActions += MemoAction(MemoOperation.SET, memoKey)
        node.conditions += Not(Has(memoKey))

        node.init()

        nodes += node

        node
    }
}

private fun String.toLiteral() = LiteralToken(this)