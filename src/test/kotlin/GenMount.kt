import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.junit.jupiter.api.Test
import java.io.File

class GenMount {
    @Test
    fun genMount() {
        val nodes = mutableListOf<DataNode>()
        val mountNode = DataNode("mount", "mount ".toLiteral(), description = "Unix mount command").also { mount ->
            nodes += mount


            DataNode("version", "-V".toLiteral()).also { version ->
                nodes += version
                mount.connections += version.toThis()
            }

            literalUniqueNode("show_labels", "Show also file system labels", "-l", "--show-labels") { showLabels ->
                nodes += showLabels
                mount.connections += showLabels.toThis()
            }

            literalUniqueNode("help", "Show help message", "-h", "--help") { help ->
                nodes += help
                mount.connections += help.toThis()
            }

            DataNode(
                "option_space",
                " ".toLiteral(),
                description = "Space between options, also connects to source"
            ).also { optionSpace ->
                nodes += optionSpace

                literalUniqueNode("all", "Mounts all file system in fstab", "-a", "--all") { all ->
                    nodes += all

                    mount.connections += all.toThis()
                    optionSpace.connections += all.toThis()
                    all.connections += optionSpace.toThis()
                }

                literalUniqueNode("no_canon", "Don't canonicalize path", "-c", "--no-canonicalize") { noCanon ->
                    nodes += noCanon

                    mount.connections += noCanon.toThis()
                    optionSpace.connections += noCanon.toThis()
                    noCanon.connections += optionSpace.toThis()
                }

                literalUniqueNode("fake", "Dry run", "-f", "--fake") { fake ->
                    nodes += fake

                    mount.connections += fake.toThis()
                    optionSpace.connections += fake.toThis()
                    fake.connections += optionSpace.toThis()
                }

                literalUniqueNode("fork", "Mount each devices in parallel", "-F", "--fork") { fork ->
                    nodes += fork

                    mount.connections += fork.toThis()
                    optionSpace.connections += fork.toThis()
                    fork.connections += optionSpace.toThis()

                    fork.conditions += Or(Has("all_short"), Has("all_long"))
                }

                literalUniqueNode("specify_fstab_path", "Specify fstab file path", "-T ", "--fstab ") { specifyPath ->
                    nodes += specifyPath

                    mount.connections += specifyPath.toThis()
                    optionSpace.connections += specifyPath.toThis()

                    DataNode(
                        "fstab_path",
                        FileToken(),
                        description = "File location for fstab file"
                    ).also { fstabPath ->
                        nodes += fstabPath

                        specifyPath.connections += fstabPath.toThis()
                        fstabPath.connections += optionSpace.toThis()
                    }
                }

                literalUniqueNode(
                    "internal_only",
                    "don't call the mount.<type> helpers",
                    "-i",
                    "--internal-only",
                ) { internalOnly ->
                    nodes += internalOnly

                    mount.connections += internalOnly.toThis()
                    optionSpace.connections += internalOnly.toThis()

                    internalOnly.connections += optionSpace.toThis()
                }

                literalUniqueNode("no_mtab", "don't write to /etc/mtab", "-n", "--no-mtab") { noMtab ->
                    nodes += noMtab

                    mount.connections += noMtab.toThis()
                    optionSpace.connections += noMtab.toThis()

                    noMtab.connections += optionSpace.toThis()
                }

                DataNode(
                    "options_mode",
                    "--options-mode ".toLiteral(),
                    description = "what to do with options loaded from fstab"
                ).also { optionsMode ->
                    nodes += optionsMode

                    mount.connections += optionsMode.toThis()
                    optionSpace.connections += optionsMode.toThis()

                    optionsMode.makeUnique()

                    // ignore, append, prepend, replace
                    DataNode(
                        "options_ignore",
                        "ignore".toLiteral(),
                        description = "ignore options in fstab"
                    ).also { ignore ->
                        nodes += ignore

                        optionsMode.connections += ignore.toThis()
                        ignore.connections += optionSpace.toThis()
                    }
                    DataNode(
                        "options_append",
                        "append".toLiteral(),
                        description = "append to options in fstab"
                    ).also { append ->
                        nodes += append

                        optionsMode.connections += append.toThis()
                        append.connections += optionSpace.toThis()
                    }
                    DataNode(
                        "options_prepend",
                        "prepend".toLiteral(),
                        description = "prepend to options in fstab"
                    ).also { prepend ->
                        nodes += prepend

                        optionsMode.connections += prepend.toThis()
                        prepend.connections += optionSpace.toThis()
                    }
                    DataNode(
                        "options_replace",
                        "replace".toLiteral(),
                        description = "replace options in fstab"
                    ).also { replace ->
                        nodes += replace

                        optionsMode.connections += replace.toThis()
                        replace.connections += optionSpace.toThis()
                    }
                }

                DataNode(
                    "options_source",
                    "--options-source ".toLiteral(),
                    description = "mount options source"
                ).also { optionsSource ->
                    nodes += optionsSource

                    optionsSource.makeUnique()
                    optionSpace.connections += optionsSource.toThis()
                    mount.connections += optionsSource.toThis()

                    val comma = DataNode("options_source_comma", ",".toLiteral())
                    nodes += comma

                    DataNode(
                        "options_source_fstab",
                        "fstab".toLiteral(),
                        description = "Read options from fstab"
                    ).also { fstab ->
                        nodes += fstab

                        fstab.makeUnique()

                        optionsSource.connections += fstab.toThis()
                        comma.connections += fstab.toThis()
                        fstab.connections += optionSpace.toThis()
                        fstab.connections += comma.toThis()

                        fstab.conditions += Not(Has("options_source_disable"))
                    }

                    DataNode(
                        "options_source_mtab",
                        "mtab".toLiteral(),
                        description = "Read options from mtab"
                    ).also { mtab ->
                        nodes += mtab

                        mtab.makeUnique()

                        optionsSource.connections += mtab.toThis()
                        comma.connections += mtab.toThis()
                        mtab.connections += optionSpace.toThis()
                        mtab.connections += comma.toThis()

                        mtab.conditions += Not(Has("options_source_disable"))
                    }

                    DataNode(
                        "options_source_disable",
                        "disable".toLiteral(),
                        description = "Read options from disable"
                    ).also { disable ->
                        nodes += disable

                        disable.makeUnique()

                        optionsSource.connections += disable.toThis()
                        comma.connections += disable.toThis()
                        disable.connections += optionSpace.toThis()

                        disable.memoActions += MemoAction(MemoOperation.SET, "options_source_disable")
                    }
                }

                DataNode(
                    "options_source_force",
                    "--options-source-force".toLiteral(),
                    description = "force use of options from fstab/mtab"
                ).also { force ->
                    nodes += force

                    mount.connections += force.toThis()
                    optionSpace.connections += force.toThis()

                    force.makeUnique()
                }

                val (optionsLong, _) = literalUniqueNode(
                    "options",
                    "comma-separated list of mount options",
                    "-o ",
                    "--options "
                ) { options ->
                    val comma = DataNode("options_comma", ",".toLiteral())

                    nodes += options
                    nodes += comma

                    mount.connections += options.toThis()
                    optionSpace.connections += options.toThis()

                    mountOption(options, comma, optionSpace, positiveName = "async", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "atime",
                        negativeName = "noatime",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "auto",
                        negativeName = "noauto",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "context",
                        nodes = nodes,
                        equalToken = StringToken
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "fscontext",
                        nodes = nodes,
                        equalToken = StringToken
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "defcontext",
                        nodes = nodes,
                        equalToken = StringToken
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "rootcontext",
                        nodes = nodes,
                        equalToken = StringToken
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "defaults",
                        nodes = nodes,
                        positiveInitializer = {
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_rw")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_suid")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_dev")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_exec")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_auto")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_nouser")
                            it.memoActions += MemoAction(MemoOperation.SET, "mount_options_async")

                            it.conditions += Not(Has("mount_options_ro"))
                            it.conditions += Not(Has("mount_options_nosuid"))
                            it.conditions += Not(Has("mount_options_nodev"))
                            it.conditions += Not(Has("mount_options_noexec"))
                            it.conditions += Not(Has("mount_options_noauto"))
                            it.conditions += Not(Has("mount_options_user"))
                        })
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "dev",
                        negativeName = "nodev",
                        nodes = nodes
                    )
                    mountOption(options,
                        comma,
                        optionSpace,
                        positiveName = "diratime",
                        negativeName = "nodiratime",
                        nodes = nodes,
                        positiveInitializer = {
                            it.conditions += Not(Has("mount_options_noatime"))
                        },
                        negativeInitializer = {
                            it.conditions += Not(Has("mount_options_noatime"))
                        }
                    )
                    mountOption(options, comma, optionSpace, positiveName = "dirsync", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "exec",
                        negativeName = "noexec",
                        nodes = nodes
                    )
                    mountOption(options, comma, optionSpace, positiveName = "group", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "iversion",
                        negativeName = "noiversion",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "mand",
                        negativeName = "nomand",
                        nodes = nodes
                    )
                    mountOption(options, comma, optionSpace, positiveName = "_netdev", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "nofail", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "realtime",
                        negativeName = "norealtime",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "strictatime",
                        negativeName = "nostrictatime",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "lazytime",
                        negativeName = "nolazytime",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "suid",
                        negativeName = "nosuid",
                        nodes = nodes
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "silent",
                        negativeName = "loud",
                        nodes = nodes
                    )
                    mountOption(options, comma, optionSpace, positiveName = "owner", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "remount", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "ro", negativeName = "rw", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "sync", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "user",
                        negativeName = "nouser",
                        nodes = nodes
                    )
                    mountOption(options, comma, optionSpace, positiveName = "users", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "nosymfollow", nodes = nodes)
                    mountOption(options, comma, optionSpace, positiveName = "novrs", nodes = nodes)
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "session",
                        nodes = nodes,
                        equalToken = NumberToken(true)
                    )
                    mountOption(
                        options,
                        comma,
                        optionSpace,
                        positiveName = "lastblock",
                        nodes = nodes,
                        equalToken = NumberToken(true)
                    )
                    DataNode("mount_options_loop", "loop".toLiteral()).also { loop ->
                        nodes += loop

                        loop.makeUnique()

                        options.connections += loop.toThis()
                        comma.connections += loop.toThis()
                        loop.connections += comma.toThis()
                        loop.connections += optionSpace.toThis()

                        val loopEqual = DataNode("mount_options_loop_equal", "=".toLiteral()).also { loopEqual ->
                            nodes += loopEqual

                            val devFilePath = DataNode("mount_options_loop_devfile", FileToken(false, false, true)).also { devFile ->
                                nodes += devFile

                                devFile.connections += comma.toThis()
                                devFile.connections += optionSpace.toThis()
                            }

                            loopEqual.connections += devFilePath.toThis()
                        }

                        loop.connections += loopEqual.toThis()

                        loop.memoActions += MemoAction(MemoOperation.SET, "mount_options_loop")
                    }

                    run {/*genForFat*/
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "uid",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "gid",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "umask",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "dmask",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "fmask",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            "allow_utime",
                            specificFS = "fat",
                            nodes = nodes,
                            equalToken = NumberToken(true)
                        )
                        DataNode("mount_options_fat_check_header", "check=".toLiteral()).also { check ->
                            nodes += check

                            check.makeUnique()
                            check.conditions += EqualTo("filesystem", "fat")
                            comma.connections += check.toThis()
                            check.connections += comma.toThis()
                            mount.connections += check.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                check.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_fat_check_relaxed", "relaxed".toLiteral()))
                            initializer(DataNode("mount_options_fat_check_normal", "normal".toLiteral()))
                            initializer(DataNode("mount_options_fat_check_strict", "strict".toLiteral()))
                        }
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "codepage",
                            nodes = nodes,
                            specificFS = "fat",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "debug",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "discard",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "dos1xfloppy",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        DataNode("mount_options_fat_errors_header", "errors=".toLiteral()).also { errors ->
                            nodes += errors

                            errors.makeUnique()
                            errors.conditions += EqualTo("filesystem", "fat")
                            comma.connections += errors.toThis()
                            errors.connections += comma.toThis()
                            mount.connections += errors.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                errors.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_fat_errors_panic", "panic".toLiteral()))
                            initializer(DataNode("mount_options_fat_errors_continue", "continue".toLiteral()))
                            initializer(DataNode("mount_options_fat_errors_remount-ro", "remount-ro".toLiteral()))
                        }
                        DataNode("mount_options_fat_fat_header", "fat=".toLiteral()).also { fat ->
                            nodes += fat

                            fat.makeUnique()
                            fat.conditions += EqualTo("filesystem", "fat")
                            comma.connections += fat.toThis()
                            fat.connections += comma.toThis()
                            mount.connections += fat.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                fat.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_fat_fat_12", "12".toLiteral()))
                            initializer(DataNode("mount_options_fat_fat_16", "16".toLiteral()))
                            initializer(DataNode("mount_options_fat_fat_32", "32".toLiteral()))
                        }
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "iocharset",
                            nodes = nodes,
                            equalToken = StringToken,
                            specificFS = "fat"
                        )
                        DataNode("mount_options_fat_nfs_header", "nfs=".toLiteral()).also { nfs ->
                            nodes += nfs

                            nfs.makeUnique()
                            nfs.conditions += EqualTo("filesystem", "fat")
                            comma.connections += nfs.toThis()
                            nfs.connections += comma.toThis()
                            mount.connections += nfs.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                nfs.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_fat_nfs_stale_rw", "stale_rw".toLiteral()))
                            initializer(DataNode("mount_options_fat_nfs_nostale_ro", "nostale_ro".toLiteral()))
                        }
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "tz=UTC",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "time_offset",
                            nodes = nodes,
                            specificFS = "fat",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "quite",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "rodir",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "showexec",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "sys_immutable",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "flush",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "usefree",
                            nodes = nodes,
                            specificFS = "fat"
                        )
                    }

                    run {/*genForNTFS*/
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "iocharset",
                            nodes = nodes,
                            specificFS = "ntfs",
                            equalToken = StringToken
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "nls",
                            nodes = nodes,
                            specificFS = "ntfs",
                            equalToken = StringToken
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "utf8",
                            nodes = nodes,
                            specificFS = "ntfs"
                        )
                        DataNode("mount_options_ntfs_uni_xlate_header", "uni_xlate=".toLiteral()).also { uni ->
                            nodes += uni

                            uni.makeUnique()
                            uni.conditions += EqualTo("filesystem", "ntfs")
                            comma.connections += uni.toThis()
                            uni.connections += comma.toThis()
                            mount.connections += uni.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                uni.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_ntfs_uni_xlate_0", "0".toLiteral()))
                            initializer(DataNode("mount_options_ntfs_uni_xlate_1", "1".toLiteral()))
                            initializer(DataNode("mount_options_ntfs_uni_xlate_2", "2".toLiteral()))
                        }
                        DataNode("mount_options_ntfs_posix_header", "posix=".toLiteral()).also { posix ->
                            nodes += posix

                            posix.makeUnique()
                            posix.conditions += EqualTo("filesystem", "ntfs")
                            comma.connections += posix.toThis()
                            posix.connections += comma.toThis()
                            mount.connections += posix.toThis()

                            val initializer: (DataNode) -> Unit = {
                                nodes += it
                                posix.connections += it.toThis()
                                it.connections += comma.toThis()
                                it.connections += optionSpace.toThis()
                            }

                            initializer(DataNode("mount_options_ntfs_posix_0", "0".toLiteral()))
                            initializer(DataNode("mount_options_ntfs_posix_1", "1".toLiteral()))
                        }
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "iod",
                            nodes = nodes,
                            specificFS = "ntfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "gid",
                            nodes = nodes,
                            specificFS = "ntfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "umask",
                            nodes = nodes,
                            specificFS = "ntfs",
                            equalToken = NumberToken(false)
                        )
                    }
                    run { /*genForusbfs*/
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "devuid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "devgid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "devmode",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "busuid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "busgid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "busmode",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "listuid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "listgid",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                        mountOption(
                            options,
                            comma,
                            optionSpace,
                            positiveName = "listmode",
                            nodes = nodes,
                            specificFS = "usbfs",
                            equalToken = NumberToken(false)
                        )
                    }
                }

                literalUniqueNode("test-opts", "limit the set of filesystems", "--test-opts ", "-O ") { test ->
                    nodes += test

                    optionSpace.connections += test.toThis()
                    mount.connections += test.toThis()
                    test.conditions += Or(Has("all_short"), Has("all_long"))
                    test.connections += optionsLong.connections
                }

                literalUniqueNode(
                    "read_only",
                    "mount the filesystem read-only (same as -o ro)",
                    "-r",
                    "--read-only"
                ) { readOnly ->
                    nodes += readOnly

                    optionSpace.connections += readOnly.toThis()
                    mount.connections += readOnly.toThis()
                    readOnly.connections += optionSpace.toThis()
                    readOnly.memoActions += MemoAction(MemoOperation.SET, "mount_options_ro")
                    readOnly.conditions += Not(Has("mount_options_ro"))
                }

                literalUniqueNode(
                    "types",
                    "limit the set of filesystem types, and also specifies it",
                    "-t ",
                    "--types "
                ) { types ->
                    nodes += types

                    optionSpace.connections += types.toThis()
                    mount.connections += types.toThis()

                    val comma = DataNode("types_comma", ",".toLiteral())
                    nodes += comma

                    listOf(
                        "fuseblk",
                        "pstore",
                        "netfs",
                        "ntfs3",
                        "coda",
                        "cgroup",
                        "hfsplus",
                        "zonefs",
                        "proc",
                        "nfs_common",
                        "freevxfs",
                        "overlayfs",
                        "qnx4",
                        "btrfs",
                        "reiserfs",
                        "devpts",
                        "qnx6",
                        "erofs",
                        "ramfs",
                        "9p",
                        "devtmpfs",
                        "fuse",
                        "fat",
                        "overlay",
                        "udf",
                        "cifs",
                        "lockd",
                        "affs",
                        "efs",
                        "smbfs_common",
                        "sysv",
                        "nls",
                        "ntfs",
                        "ext4",
                        "cachefiles",
                        "hfs",
                        "ksmbd",
                        "binfmt_misc",
                        "bdev",
                        "ecryptfs",
                        "ocfs2",
                        "isofs",
                        "exfat",
                        "fusectl",
                        "romfs",
                        "minix",
                        "cramfs",
                        "jffs2",
                        "omfs",
                        "ubifs",
                        "sockfs",
                        "configfs",
                        "hugetlbfs",
                        "fscache",
                        "bpf",
                        "afs",
                        "nilfs2",
                        "adfs",
                        "cgroup2",
                        "securityfs",
                        "ufs",
                        "ext2",
                        "hpfs",
                        "pipefs",
                        "mqueue",
                        "vboxsf",
                        "xfs",
                        "jfs",
                        "efivarfs",
                        "debugfs",
                        "gfs2",
                        "sysfs",
                        "dlm",
                        "orangefs",
                        "quota",
                        "nfsd",
                        "nfs",
                        "vfat",
                        "befs",
                        "binfmt_misc.ko",
                        "autofs",
                        "squashfs",
                        "bfs",
                        "cpuset",
                        "ext3",
                        "ceph",
                        "f2fs",
                        "tmpfs"
                    ).forEach { fsType ->
                        fs(types, comma, optionSpace, fsType, nodes)
                    }
                }

                literalUniqueNode("verbose", "say what is being done", "-v", "--verbose") { verbose ->
                    nodes += verbose

                    mount.connections += verbose.toThis()
                    optionSpace.connections += verbose.toThis()
                    verbose.connections += optionSpace.toThis()
                }

                literalUniqueNode(
                    "read_write",
                    "mount the filesystem read-write",
                    "-w",
                    "--rw",
                    "--read-write"
                ) { readWrite ->
                    nodes += readWrite

                    mount.connections += readWrite.toThis()
                    optionSpace.connections += readWrite.toThis()
                    readWrite.connections += optionSpace.toThis()

                    readWrite.memoActions += MemoAction(MemoOperation.SET, "mount_options_rw")
                    readWrite.conditions += Not(Has("mount_options_ro"))
                }

                literalUniqueNode(
                    "namespace",
                    "perform mount in another namespace",
                    "-N ",
                    "--namespace "
                ) { namespace ->
                    nodes += namespace

                    mount.connections += namespace.toThis()
                    optionSpace.connections += namespace.toThis()

                    val contents =
                        DataNode("namespace_contents", StringToken, connections = mutableListOf(optionSpace.toThis()))
                    nodes += contents

                    namespace.connections += contents.toThis()
                }

                val spaceAfterSource = DataNode("space_after_source", " ".toLiteral())
                nodes += spaceAfterSource

                literalWithSharedMemo("source", "explicitly specifies source", "--source ".toLiteral(), "has_source") { sources ->
                    nodes += sources

                    mount.connections += sources.toThis()
                    optionSpace.connections += sources.toThis()

                    val deviceFileNode = DataNode("source_devfile", FileToken(false, false, true))
                    nodes += deviceFileNode

                    sources.connections += deviceFileNode.toThis()

                    deviceFileNode.connections += spaceAfterSource.toThis()
                }

                literalUniqueNode("target", "explicitly specifies mountpoint", "--target ") { target ->
                    nodes += target

                    mount.connections += target.toThis()
                    optionSpace.connections += target.toThis()
                    spaceAfterSource.connections += target.toThis()

                    val targetDirectory = DataNode("target_directory", FileToken(true, false, false))
                    nodes += targetDirectory

                    target.connections += targetDirectory.toThis()
                    spaceAfterSource.connections += targetDirectory.toThis()

                    targetDirectory.connections += optionSpace.toThis()
                }

                val sourceLabelNode = DataNode("label_source_contents", StringToken)
                val sourceUUIDNode = DataNode("uuid_source_contents", StringToken)
                nodes += sourceLabelNode
                nodes += sourceUUIDNode

                sourceLabelNode.connections += spaceAfterSource.toThis()
                sourceUUIDNode.connections += spaceAfterSource.toThis()

                literalWithSharedMemo(
                    "label_source_0",
                    "specifies device by filesystem label",
                    "-L".toLiteral(),
                    "has_source"
                ) { labelSource ->
                    nodes += labelSource

                    mount.connections += labelSource.toThis()
                    optionSpace.connections += labelSource.toThis()

                    labelSource.connections += sourceLabelNode.toThis()
                }

                literalWithSharedMemo(
                    "label_source_1",
                    "specifies device by filesystem label",
                    "LABEL=".toLiteral(),
                    "has_source"
                ) { labelSource ->
                    nodes += labelSource

                    mount.connections += labelSource.toThis()
                    optionSpace.connections += labelSource.toThis()

                    labelSource.connections += sourceLabelNode.toThis()
                }

                literalWithSharedMemo("uuid_source_0", "specifies device by filesystem uuid", "-U".toLiteral(), "has_source") { uuidSource ->
                    nodes += uuidSource

                    mount.connections += uuidSource.toThis()
                    optionSpace.connections += uuidSource.toThis()

                    uuidSource.connections += sourceUUIDNode.toThis()
                }

                literalWithSharedMemo("uuid_source_1", "specifies device by filesystem uuid", "UUID=".toLiteral(), "has_source") { uuidSource ->
                    nodes += uuidSource

                    mount.connections += uuidSource.toThis()
                    optionSpace.connections += uuidSource.toThis()

                    uuidSource.connections += sourceUUIDNode.toThis()
                }

                literalWithSharedMemo(
                    "part_label_source",
                    "specifies device by partition label",
                    "PARTLABEL=".toLiteral(),
                    "has_source"
                ) { partLabelSource ->
                    nodes += partLabelSource

                    mount.connections += partLabelSource.toThis()
                    optionSpace.connections += partLabelSource.toThis()

                    partLabelSource.connections += sourceLabelNode.toThis()
                }

                literalWithSharedMemo(
                    "part_uuid_source",
                    "specifies device by partition uuid",
                    "PARTUUID=".toLiteral(),
                    "has_source"
                ) { partUUIDSource ->
                    nodes += partUUIDSource

                    mount.connections += partUUIDSource.toThis()
                    optionSpace.connections += partUUIDSource.toThis()

                    partUUIDSource.connections += sourceUUIDNode.toThis()
                }

                literalWithSharedMemo("path_devfile_source", "specifies device by path", FileToken(false, false, true), "has_source") { devFile ->
                    nodes += devFile

                    mount.connections += devFile.toThis()
                    optionSpace.connections += devFile.toThis()

                    devFile.connections += spaceAfterSource.toThis()
                }

                literalWithSharedMemo("path_dir_source", "mountpoint for bind mounts", FileToken(true, false, false), "has_source") { dir ->
                    nodes += dir

                    mount.connections += dir.toThis()
                    optionSpace.connections += dir.toThis()

                    dir.connections += spaceAfterSource.toThis()

                    dir.conditions += Has("mount_options_bind")
                }

                literalWithSharedMemo("path_file_source", "regular file for loopdev setup", FileToken(false, true, false), "has_source") { file ->
                    nodes += file

                    mount.connections += file.toThis()
                    optionSpace.connections += file.toThis()

                    file.connections += spaceAfterSource.toThis()

                    file.conditions += Has("mount_options_loop")
                }
            }

            DataNode("operation_space", " ".toLiteral()).also { operationSpace ->
                nodes += operationSpace

                val mountPoint = DataNode("operation_mountpoint", FileToken(true, false, false))
                nodes += mountPoint

                val spaceAfterMountPoint = DataNode("space_after_mount_point", " ".toLiteral())
                nodes += spaceAfterMountPoint

                mountPoint.connections += spaceAfterMountPoint.toThis()

                val target = DataNode("operation_target", FileToken(true, false, false))
                nodes += target

                spaceAfterMountPoint.connections += target.toThis()


                val operations = listOf(
                    "-B " to "mount a subtree somewhere else (same as -o bind)",
                    "--bind " to "mount a subtree somewhere else (same as -o bind)",
                    "-M " to "move a subtree to some other place",
                    "--move " to "move a subtree to some other place",
                    "-R " to "mount a subtree and all submounts somewhere else",
                    "--rbind " to "mount a subtree and all submounts somewhere else",
                    "--make-shared " to "mark a subtree as shared",
                    "--make-slave " to "mark a subtree as slave",
                    "--make-private " to "mark a subtree as private",
                    "--make-unbindable " to "mark a subtree as unbindable",
                    "--make-rshared " to "recursively mark a whole subtree as shared",
                    "--make-rslave " to "recursively mark a whole subtree as slave",
                    "--make-rprivate " to "recursively mark a whole subtree as private",
                    "--make-runbindable " to "recursively mark a whole subtree as unbindable"
                )

                operations.forEachIndexed { index, (literal, description) ->
                    literalWithSharedMemo("operation_$index", description, literal.toLiteral(), "operation") { operation ->
                        nodes += operation

                        mount.connections += operation.toThis()

                        operation.connections += mountPoint.toThis()
                    }
                }
            }
        }
        val sudoNotifier = Tip(Or(Has("options_0"), Has("options_1")), "This command needs super user permission to run")
        val command = Command("mount", nodes, mountNode.identifier, listOf(sudoNotifier))
        File("src/test/resources/mount.json").writeText(Json.encodeToString(command))
    }
}

private fun String.toLiteral() = LiteralToken(this)

private fun DataNode.makeUnique() {
    val memoAction = MemoAction(MemoOperation.SET, identifier.replace(' ', '_'))

    val condition = Not(Has(memoAction.name))

    memoActions += memoAction
    conditions += condition
}

private fun literalUniqueNode(
    identifier: String,
    description: String,
    vararg literals: String,
    initializer: (DataNode) -> Unit,
): List<DataNode> {
    return literals.mapIndexed { index, name ->
        val indexTag = if (literals.size == 1) "" else "_$index"
        DataNode(identifier = "${identifier}$indexTag", token = name.toLiteral(), description = description).also {
            initializer(it)

            it.makeUnique()

            for (toIgnore in 0..literals.lastIndex) {
                if (toIgnore != index) it.conditions += Not(Has("${identifier}_$toIgnore"))
            }
        }
    }
}

private fun literalWithSharedMemo(
    identifier: String,
    description: String,
    token: IToken,
    memoKey: String,
    initializer: (DataNode) -> Unit
): DataNode {
    val node = DataNode(identifier, token, description = description)
    node.memoActions += MemoAction(MemoOperation.SET, memoKey)
    node.conditions += Not(Has(memoKey))
    initializer(node)
    return node
}

private fun mountOption(
    optionsNode: DataNode,
    comma: DataNode,
    optionSpace: DataNode,
    positiveName: String,
    negativeName: String? = null,
    positiveDescription: String? = null,
    negativeDescription: String? = null,
    positiveInitializer: (DataNode) -> Unit = {},
    negativeInitializer: (DataNode) -> Unit = {},
    specificFS: String? = null,
    nodes: MutableList<DataNode>,
    equalToken: IToken? = null
): Pair<DataNode, DataNode?> {
    val fsTag = if (specificFS == null) "" else "${specificFS}_"
    if (negativeName != null) {
        assert(equalToken == null)
        val positive =
            DataNode("mount_options_$fsTag${positiveName}", positiveName.toLiteral(), description = positiveDescription)
        positive.makeUnique()
        positiveInitializer(positive)
        optionsNode.connections += positive.toThis()
        comma.connections += positive.toThis()
        positive.connections += comma.toThis()
        positive.connections += optionSpace.toThis()
        positive.memoActions += MemoAction(MemoOperation.SET, "mount_options_$fsTag$positiveName")
        positive.conditions += Not(Has("mount_options_$fsTag$negativeName"))

        val negative =
            DataNode("mount_options_$fsTag${negativeName}", negativeName.toLiteral(), description = negativeDescription)
        negative.makeUnique()
        negativeInitializer(negative)
        optionsNode.connections += negative.toThis()
        comma.connections += negative.toThis()
        negative.connections += comma.toThis()
        negative.connections += optionSpace.toThis()
        negative.memoActions += MemoAction(MemoOperation.SET, "mount_options_$fsTag$negativeName")
        negative.conditions += Not(Has("mount_options_$fsTag$positiveName"))

        if (specificFS != null) {
            positive.conditions += EqualTo("filesystem", specificFS)
            negative.conditions += EqualTo("filesystem", specificFS)
        }

        nodes += positive
        nodes += negative

        return positive to negative
    } else {
        if (equalToken != null) {
            val mountOption = DataNode(
                "mount_options_$fsTag${positiveName}_header",
                "${positiveName}=".toLiteral(),
                description = positiveDescription
            )
            mountOption.makeUnique()
            positiveInitializer(mountOption)

            val valueNode =
                DataNode("mount_options_$fsTag${positiveName}_value", equalToken, description = positiveDescription)
            optionsNode.connections += mountOption.toThis()
            comma.connections += mountOption.toThis()

            mountOption.connections += valueNode.toThis()

            valueNode.connections += comma.toThis()
            valueNode.connections += optionSpace.toThis()

            if (specificFS != null) {
                mountOption.conditions += EqualTo("filesystem", specificFS)
            }

            nodes += mountOption
            nodes += valueNode

            return mountOption to null
        } else {
            val mountOption = DataNode(
                "mount_options_$fsTag${positiveName}",
                positiveName.toLiteral(),
                description = positiveDescription
            )
            mountOption.makeUnique()
            positiveInitializer(mountOption)
            optionsNode.connections += mountOption.toThis()
            comma.connections += mountOption.toThis()
            mountOption.connections += comma.toThis()
            mountOption.connections += optionSpace.toThis()

            if (specificFS != null) {
                mountOption.conditions += EqualTo("filesystem", specificFS)
            }

            nodes += mountOption

            return mountOption to null
        }
    }
}

private fun fs(
    typeNode: DataNode,
    commaNode: DataNode,
    optionSpace: DataNode,
    fsName: String,
    nodes: MutableList<DataNode>
): DataNode {
    val fs = DataNode("mount_fs_$fsName", fsName.toLiteral()).also { fs ->
        fs.makeUnique()

        fs.connections += commaNode.toThis()
        fs.connections += optionSpace.toThis()

        typeNode.connections += fs.toThis()
        commaNode.connections += fs.toThis()

        fs.memoActions += MemoAction(MemoOperation.APPEND, "filesystem", "$fsName ")

        fs.conditions += Not(Contains("filesystem", "no$fsName"))

        nodes += fs
    }

    DataNode("mount_fs_no$fsName", fsName.toLiteral()).also { noFs ->
        noFs.makeUnique()

        noFs.connections += commaNode.toThis()
        noFs.connections += optionSpace.toThis()

        typeNode.connections += noFs.toThis()
        commaNode.connections += noFs.toThis()

        noFs.memoActions += MemoAction(MemoOperation.APPEND, "filesystem", "no$fsName ")

        noFs.conditions += Not(Contains("filesystem", fsName))

        nodes += noFs
    }

    return fs
}